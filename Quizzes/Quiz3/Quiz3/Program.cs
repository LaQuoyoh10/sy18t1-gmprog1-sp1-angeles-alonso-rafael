﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3
{
    class Program
    {
        static void Main(string[] args)
        {
            Loop3();
        }

        //PYRAMID
        static void Loop1()
        {
            Console.WriteLine("Enter a number: ");
            int number = int.Parse(Console.ReadLine());

            for (int x = 0; x < number; x++)
            {
                Console.Write("*");
                for(int y =0; y < x; y++)
                {
                   Console.Write("*");
                }
                Console.WriteLine();
            }
               
            
        }
        // BOARD
        static void Loop2()
        {
            for (int x = 0; x < 6; x++)
            {
              
                for (int y = 0; y < 8; y++)
                {
                  
                    if (y % 2 == 0)
                    {
                      Console.Write("X");
                    }
                    else
                     Console.Write("O");
                }
                Console.WriteLine();
            }
        }

        //DICE
        static void Loop3()
        {
            Random rand = new Random();
            bool isFactor = false;
            int number = 1;
           while(isFactor == false)
            {
            int dice1 = rand.Next(1, 6);
            int dice2 = rand.Next(1, 6);
            int total = dice1 + dice2;
             
                Console.WriteLine(dice1+","+dice2);
                if (total % 4 == 0)
                {
                    Console.WriteLine("You rolled " + number + " time(s)");
                    isFactor = true;
                }
                else
                    number++;
                    Console.WriteLine();
            }
            Console.WriteLine();

        }


    }
}
