﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class Program
    {
        static void Main(string[] args)
        {
            Character player = CreateCharacter();
            while ((player.Alive == true) && (player.HasMoney == true) && (player.NotWon == true))
            {
                DrawRound(player);
            }
            if((player.Alive == false)&&(player.HasMoney==false))
            {
                DeadnBroke(player);
            }
            else if(player.Alive == false)
            {
                Death(player);
            }
            else if (player.HasMoney == false)
            {
                Broke(player);
            }
            else
            {
                Won(player);
            }
        }

        static Character CreateCharacter()
        {
            
            Console.Write("Input your name: ");

            Character character = new Character()
            {
                Name = Console.ReadLine(),
                CurrentHP = 100,
                Currency =100,
                Points =0

            };
            return character;
        }
        static void DrawRound(Character character)
        {
            character.DisplayAll();
            Console.ReadLine();
            character.Draw();
        }
        static void Death(Character character)
        {
            Console.WriteLine("YOU HAVE DIED!");
            character.DisplayAll();
            Console.ReadLine();
            System.Environment.Exit(1);
        }
        static void Broke(Character character)
        {
            Console.WriteLine("UNFORTUNATELY YOU HAVE NO MORE CRYSTALS!");
            character.DisplayAll();
            Console.ReadLine();
            System.Environment.Exit(1);
        }
        static void DeadnBroke(Character character)
        {
            Console.WriteLine("WOW...YOU ARE DEAD AND BROKE");
            character.DisplayAll();
            Console.ReadLine();
            System.Environment.Exit(1);
        }
        static void Won(Character character)
        {
            Console.WriteLine("YOU HAVE WON!");
            character.DisplayAll();
            Console.ReadLine();
            System.Environment.Exit(1);
        }

    }
}
