﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class Crystal : Item
    {
        private int Money = 15;
        public override void Use(Character user)
        {
            Console.WriteLine("LUCKY YOU! You found a " + Name +"!");
            user.GetCrystals(Money);
        }
}
}
