﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class R : Item
    {
        private int points = 1;
        public override void Use(Character user)
        {
            Console.WriteLine("You found a " + Name + "!");
            user.GetPoints(points);
        }
    }
}
