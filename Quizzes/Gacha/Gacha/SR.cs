﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class SR : Item
    {
        private int points = 10;
        public override void Use(Character user)
        {
            Console.WriteLine("GREAT! You found a " + Name + "!");
            user.GetPoints(points);
        }
    }
}
