﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class Item
    {
        private string ItemName;
        public string Name
        { get { { return ItemName; } }
          set
            {
                ItemName = value;
            }
        }
        //USES ITEM FOUND
        public virtual void Use(Character user)
        {
            Console.WriteLine("You got something!");
        }
    }
}
