﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class HealthPotion : Item
    {
        private int Health = 30;
        public override void Use(Character user)
        {
            Console.WriteLine("NICE! You found a " + Name + "!");
            user.Heal(Health);
        }
    }
}
