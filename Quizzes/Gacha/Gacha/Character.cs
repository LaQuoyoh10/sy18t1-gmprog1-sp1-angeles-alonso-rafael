﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class Character
    {
        private int _currency;
        private int _points;
        private int _currentHp;

        Random rand = new Random();
        private List<Item> items = new List<Item>();
        public IEnumerable<Item> Members { get { return items; } }

        public string Name { get; set;}
        public bool Alive
        {
            get
            {
                if (_currentHp < 0)
                {
                    _currentHp = 0;
                }
                return _currentHp > 0; }
        }
        public bool HasMoney
        {
            get { return _currency > 0; }
        }
        public bool NotWon
        {
            get { return _points < 100; }
        }
        public int Currency
        { get {return _currency; }
          set
            {
                _currency = value;
            }
        }
        public int Points
        { get { return _points;}
            set
            {
                _points = value;
            }
        }
        public int CurrentHP
        { get { return _currentHp; }
            set
            {
                _currentHp = value;
                if (_currentHp < 0)
                {
                    _currentHp = 0;
                }
            }
        }

        public void DisplayStats()
        {
            Console.WriteLine("Name : " + Name);
            Console.WriteLine("Hp : " + CurrentHP);
            Console.WriteLine(" Crystals : " + Currency);
            Console.WriteLine(" Points : " + Points);
        }
        public void Draw()
        {
            _currency -= 5;
            double ItemDraw = rand.NextDouble();
            if(ItemDraw >= 0 && ItemDraw <= 0.39)
            {
                R r = new R()
                {
                    Name = "R"
                };
                r.Use(this);
                items.Add(r);
            }
            else if(ItemDraw >= 0.40 && ItemDraw <= 0.48)
            {
                SR sr = new SR()
                {
                    Name = "SR"
                };
                sr.Use(this);
                items.Add(sr);
            }
            else if (ItemDraw >= 0.49 && ItemDraw <= 0.63)
            {
                HealthPotion healthPotion = new HealthPotion()
                {
                    Name = "Health Potion"
                };
                healthPotion.Use(this);
                items.Add(healthPotion);
            }
            else if (ItemDraw >= 0.64 && ItemDraw <= 0.83)
            {
                Bomb bomb = new Bomb()
                {
                    Name = "Bomb"
                };
                bomb.Use(this);
                items.Add(bomb);
            }
            else if (ItemDraw >= 0.84 && ItemDraw < 0.98)
            {
                Crystal crystal = new Crystal()
                {
                    Name = "Crystal"
                };
                crystal.Use(this);
                items.Add(crystal);
            }
            else
            {
                SSR ssr = new SSR()
                {
                    Name = "SSR"
                };
                ssr.Use(this);
                items.Add(ssr);
            }
        }
        public void DisplayItems()
        {

            for (int i = 0; i < 6; i++)
            {
                string name;
                if (i == 0)
                {
                    name = "Health Potion";
                }
                else if (i == 1)
                {
                    name = "Crystal";
                }
                else if (i == 2)
                {
                    name = "SSR";
                }
                else if (i == 3)
                {
                    name = "SR";
                }
                else if (i == 4)
                {
                    name = "R";
                }
                else
                {
                    name = "Bomb";
                }
                int count = 0;

                for (int x = 0; x < items.Count; x++)
                    if (items[x].Name == name)
                    {
                        count++;
                    }

                Console.WriteLine(i + 1 + ".) " + name + " : " + count);
            }

        }
        public void Heal(int health)
        {
            Console.WriteLine("You got healed for " + health);
            _currentHp += health;
        }
        public void GetPoints(int points)
        {
            if(points == 1) Console.WriteLine("You got " + points + " POINT!");
            else
            {
                Console.WriteLine("You got " + points + " POINTS!");
            }
            _points += points;
        }
        public void DMG(int dmg)
        {
            Console.WriteLine("OUCH! You got " + dmg + " DMG!");
            _currentHp -= dmg;
        }
        public void GetCrystals(int money)
        {
            Console.WriteLine("You got " + money + " CRYSTALS!");
            _currency += money;
        }
        public void DisplayAll()
        {
            this.DisplayStats();
            this.DisplayItems();
        }
    }
}
