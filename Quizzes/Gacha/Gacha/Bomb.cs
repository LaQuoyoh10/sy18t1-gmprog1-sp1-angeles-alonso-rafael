﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gacha
{
    class Bomb : Item
    {
        private int damage = 25;
        public override void Use(Character user)
        {
            Console.WriteLine("OH NO! You found a " + Name + "!");
            user.DMG(damage);
        }
    }
}
