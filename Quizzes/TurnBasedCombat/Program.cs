﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    class Program
    {
        // You don't need to do anything here
        static void Main(string[] args)
        {
            // Prepare team
            Team player = CreatePlayerTeam();
            Team enemy = CreateEnemyTeam();
            
            // Initiate combat
            Combat combat = new Combat();
            combat.Player = player;
            combat.Enemy = enemy;
            combat.Initiate(); // This will not finish until a winner is determined

            // Combat has ended at this point
            Console.ReadKey();
        }

        /// <summary>
        /// Create a Team of PlayerUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Player Team</returns>
        static Team CreatePlayerTeam()
        {

            Team team = new Team();
            Console.Write("Input the name of this party : ");
            team.Name = Console.ReadLine();
            
            Console.WriteLine();
            for (int x = 0; x < 3; x++)
            {
                Console.Write("Input the name of this unit : ");
                string name = Console.ReadLine();
                Unit unit = new PlayerUnit()
                {
                    Name = name,
                    CurrentHp = 150

                };
                team.AddMember(unit);
            }
            return team;

        }

        /// <summary>
        /// Create a Team of EnemyUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action.
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Enemy Team</returns>
        static Team CreateEnemyTeam()
        {
            throw new NotImplementedException();
        }
    }
}
