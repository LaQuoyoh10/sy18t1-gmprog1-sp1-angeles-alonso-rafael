﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class SingleTargetDamageAction : Action
    {
        public SingleTargetDamageAction(Unit unit)
            : base(unit)
        {
        }

        public float DamageMultiplier { get; set; }

        /// <summary>
        /// Apply damage to opposing (team) unit with the lowest HP
        /// Use Combat.TurnOrder to get ALL units
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            throw new NotImplementedException();
        }
    }
}
