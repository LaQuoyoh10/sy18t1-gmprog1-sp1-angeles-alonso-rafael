﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class MultiTargetDamageAction : Action
    {
        public MultiTargetDamageAction(Unit actor)
            : base(actor)
        {
        }

        public float DamageMultiplier { get; set; }

        /// <summary>
        /// Apply damage to ALL opposing units (team). Each damage must be evaluated separately (different damage)
        /// Use Combat.TurnOrder to get ALL units (this includes your ally units as well)
        /// 
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            throw new NotImplementedException();
        }
    }
}
