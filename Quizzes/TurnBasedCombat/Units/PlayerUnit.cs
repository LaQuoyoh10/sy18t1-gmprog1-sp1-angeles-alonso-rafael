﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class PlayerUnit : Unit
    {
        /// <summary>
        /// Evaluate the action to be used for the PlayerUnit
        /// 1. Display the Unit
        /// 2. Create a list of valid actions (enough MP)
        /// 3. Print the list to display the actions the user can take
        /// 4. Get user input for action selection. Make sure that the user is inputting valid commands
        /// 5. Clear the console
        /// 6. Execute the action (Action.Execute). Remember to pass in the combat parameter to Action.Execute.
        /// </summary>
        /// <param name="combat"></param>
        public override void EvaluateTurn(Combat combat)
        {
            DisplayUnit();
            Console.WriteLine("What would you like to do? : ");
            foreach(Action actions in Actions)
            {
                Console.WriteLine(Name);
            }
            int response = Int32.Parse(Console.ReadLine());
            if (response == 1)
            {
                 
            }
            else if (response == 2)
            {
               
            }
            else
            {
                
            }
        }
    }
}
