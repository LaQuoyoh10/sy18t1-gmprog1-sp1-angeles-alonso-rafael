﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    /// <summary>
    /// Nothing to do here
    /// </summary>
    public struct Stats
    {
        public int Hp { get; set; }
        public int Mp { get; set; }
        public int Power { get; set; }
        public int Vitality { get; set; }
        public int Agility { get; set; }
        public int Dexterity { get; set; }

        public void DisplayStats()
        {
            Console.WriteLine("HP: " + Hp);
            Console.WriteLine("MP: " + Mp);
            Console.WriteLine("POW: " + Power);
            Console.WriteLine("VIT: " + Vitality);
            Console.WriteLine("AGI: " + Agility);
            Console.WriteLine("DEX: " + Dexterity);
        }
    }
}
