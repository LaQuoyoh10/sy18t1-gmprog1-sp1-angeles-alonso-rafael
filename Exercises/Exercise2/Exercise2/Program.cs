﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            Exer4();
        }

        static void Exer1()
        {
            int[] numbers1 = { 1, 2, 3, 4, 5 };
            int[] numbers2 = { 0, 7, -10, 5, 3 };
            int[] numbers3 = new int[5];
            for (int x = 0; x < 5; x++)
            {
                Console.Write(numbers1[x] + ",");
                
            }
            Console.WriteLine();
            for (int y = 0; y < 5; y++)
            {
                Console.Write(numbers2[y]+",");

            }
            for (int z =0; z< 5; z++)
            {
                numbers3[z] = numbers1[z] + numbers2[z];
            }
            Console.WriteLine();
            Console.WriteLine("=");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(numbers3[i] + ",");

            }
            Console.Read();
        }

        static void Exer2()
        {
            string[] items = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
            Console.Write("Input item that you like to check: ");
            string name = Console.ReadLine();
            int count = 0;
            for(int x =0; x < 8; x++)
            {
                if(items[x] == name)
                {
                    count++;
                }
            }
            Console.WriteLine("You have " + count +" "+ name + "(s)");
            Console.Read();

        }

        static void Exer3()
        {
            Random rand = new Random();
            bool willExit = false;
            string[] items = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
            while (willExit == false)
            {
                  int itemRand = rand.Next(0, 7);
                  Console.WriteLine(items[itemRand]);
                  Console.Write("Would you like to continue? (Press 'x' || 'X to exit) ");
                  string response = Console.ReadLine();
                if((response == "x")||(response == "X"))
                {
                    willExit = true;
                }
            }
            Console.Read();

        }

        static void Exer4()
        {
            int[] numbers = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };
            int x = numbers.Max();
            for(int y =0; y < 10; y++)
            {
                Console.Write(numbers[y]+",");
            }
            Console.WriteLine();
            Console.Write("The highest value is : " + x);

            Console.Read();

        }



    }
}
