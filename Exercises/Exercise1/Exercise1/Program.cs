﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            Exer4();
        }

        static void Exer1()
        {
            Console.Write("Input number of values to be computed: ");
            int amount= Int32.Parse(Console.ReadLine());
            int value = 0;
            int round = 1;
            for(int x =0; x < amount; x++)
            {
                
                Console.Write("Input value "+ round + ": ");
                int numbers = Int32.Parse(Console.ReadLine());
                value += numbers;
                round++;
            }
            int average = value / amount;
            Console.WriteLine("Average :" + average);
            Console.Read();
        }

        static void Exer2()
        {
            Console.Write("Input number: ");
            int number = Int32.Parse(Console.ReadLine());
            int factorial = 1;

            if(number != 0)
            {
                number++;
                 for (int x = 1; x < number; x++)
                   {
                       factorial *= x;
                   }
                number--;
            }
            else
            {
                factorial = 1;
            }
            
            Console.WriteLine("The Factorial of " +number+" is " +factorial);
            Console.Read();
        }

        static void Exer3()
        {
            Console.Write("Input number: ");
            int number = Int32.Parse(Console.ReadLine());
            Console.Write("Input number of values to be computed: ");
            int amount = Int32.Parse(Console.ReadLine());
            int numbers = number + amount;
            int value = 0;
            
            for (int x = number; x < numbers; x++)
            {
          
                    Console.Write(number + "+");
                    
             
                    value += number;
                    number++;
            }
            Console.Write("=" + value);
            Console.Read();
        }

        static void Exer4()
        {
            Console.Write("Input number of terms: ");
            int number = Int32.Parse(Console.ReadLine());
          
            int value = number * 2;

            for (int x = 0; x < value; x++)
            {
                if(x % 2 != 0)
                {
                   Console.Write(x + ",");
                }
              

            }
            Console.Read();

        }
    }
}
