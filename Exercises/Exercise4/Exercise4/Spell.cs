﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise4
{
    class Spell
    {
        private string name;
        private int mpCost;
        private int dmg;
        public Spell(string designation, int damage, int cost)
        {
            dmg = damage;
            mpCost = cost;
            name = designation;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public int MpCost
        {
            get
            {
                return mpCost;
            }
       
          
        }

        public int DMG
        {
            get
            {
                return dmg;
            }
        }

       
        
    }
}
