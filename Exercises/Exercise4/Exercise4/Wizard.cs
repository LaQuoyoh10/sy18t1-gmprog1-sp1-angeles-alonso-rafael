﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Exercise4
{
    class Wizard
    {

        private string name;
        private int hp;
        private int mp;
        public bool isAlive = true;
        public bool hasMana = true;

        public Wizard()
        {
            hp = 100;
            mp = 75;
            name = "Enemy";
        }
        public Wizard(string designation)
        {
            hp = 100;
            mp = 75;
            name = designation;
        }

        public int Hp
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
            }

        }

        public int Mp
        {
            get
            {
                return mp;
            }
            set
            {
                mp = value;
            }

        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }

        }

        static void nameWizard(Wizard wizard)
        {
           wizard.Name = Console.ReadLine();
        }

        public void castSpell(Wizard caster, Wizard target, Spell spell)
        {
            Console.WriteLine(caster.Name + " casts " + spell.Name + " at " + target.Name);
            caster.Mp -= spell.MpCost;
            takeDmg(target, spell);
        }
         static void takeDmg(Wizard victim, Spell spell)
        {
            victim.Hp -= spell.DMG;
            Console.WriteLine(victim.Name + " took " + spell.DMG + " DMG");
            Console.WriteLine(victim.Name + "'s HP : " + victim.Hp);
        }
    } 
    
}
