﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise4
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.Write("Whats your name? : ");
            string name = Console.ReadLine();
            Wizard player = new Wizard(name);
            Wizard enemy = new Wizard();
            
            Spell fireball = new Spell("Fireball", 15, 10);
            showStats(ref player);
            showStats(ref enemy);
            Console.ReadLine();
            while ((player.isAlive) && (enemy.isAlive) && (player.hasMana) && (enemy.hasMana))
            {
                attack(ref player, ref enemy, fireball);
                
                if (enemy.Hp <= 0)
                {
                    enemy.Hp = 0;
                    enemy.isAlive = false;
                    win(player, ref enemy);
                }
                if (enemy.Mp <= 0)
                {
                    enemy.Mp = 0;
                    enemy.hasMana = false;
                    win(player, ref enemy);
                }
                attack(ref enemy, ref player, fireball);

                if (player.Hp <= 0)
                {
                    player.Hp = 0;
                    player.isAlive = false;
                    lose(ref player);
                }
                 if (player.Mp <= 0)
                {
                    player.Mp = 0;
                    player.hasMana = false;
                    lose(ref player);
                }
                Console.ReadLine();
                showStats(ref player);
                showStats(ref enemy);
                Console.ReadLine();
            }
        }

        static void attack(ref Wizard attacker, ref Wizard target,Spell spell)
        {
            attacker.castSpell(attacker, target, spell);

        }

        static void win(Wizard user, ref Wizard enemy)
        {
            Console.WriteLine();
            if(enemy.isAlive == false)
            {
                Console.WriteLine("Your opponent has died!");
            }
            else
            {
                Console.WriteLine("Your opponent has no more mana!");
            }
            Console.WriteLine("YOU WON!");
            showStats(ref user);
            Console.ReadLine();
            System.Environment.Exit(1);
        }

        static void showStats(ref Wizard wizard)
        {
            Console.WriteLine("Name : "+ wizard.Name);
            Console.Write(" HP : " + wizard.Hp);
            Console.WriteLine(" MP: " + wizard.Mp);
        }
        static void lose(ref Wizard user)
        {
            Console.WriteLine();
            if (user.isAlive == false)
            {
                Console.WriteLine("You died!");
            }
            else
            {
                Console.WriteLine("You have no more mana!");
            }
            Console.WriteLine("YOU LOST!");
            showStats(ref user);
            Console.ReadLine();
            System.Environment.Exit(1);
        }
    }
}
