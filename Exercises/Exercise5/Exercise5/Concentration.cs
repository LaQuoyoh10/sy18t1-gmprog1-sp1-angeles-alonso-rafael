﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Concentration : Skills
    {
        public override void UseSkill(Unit unit) 
        {
            Console.WriteLine("You used " + Name);
            unit.Dex += 2;
        }
    }
}
