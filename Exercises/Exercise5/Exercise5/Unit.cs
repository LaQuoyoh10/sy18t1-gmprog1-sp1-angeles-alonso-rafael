﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Unit
    {
        //Stats
        //private int hp;
        //private int str;
        //private int vit;
        //private int dex;
        //private int agi;

    public List<Skills> skillList = new List<Skills>();

        //Getter and Setters
        public int Hp { get; set; }
        public int Str { get; set; }
        public int Vit { get; set; }
        public int Dex { get; set; }
        public int Agi { get; set; }

        //Functions
        public void DisplayStats()
        {
            Console.WriteLine("Hp: " + Hp);
            Console.WriteLine(" Str: " + Str);
            Console.WriteLine(" Vit: " + Vit);
            Console.WriteLine(" Dex: " + Dex);
            Console.WriteLine(" Agi: " + Agi);
        }
        
        public void CastSkill(int number)
        {
            skillList[number].UseSkill(this);
        }

    }
}
