﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class IronSkin : Skills
    {
        public override void UseSkill(Unit unit)
        {
            Console.WriteLine("You used " + Name);
            unit.Vit += 2;
        }
    }
}
