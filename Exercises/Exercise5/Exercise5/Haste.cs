﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Haste : Skills
    {
        public override void UseSkill(Unit unit)
        {
            Console.WriteLine("You used " + Name);
            unit.Agi += 2;
        }
    }
}
