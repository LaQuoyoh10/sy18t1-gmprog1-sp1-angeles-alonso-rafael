﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Program
    {
        static Random rand = new Random();
   
        static void Main(string[] args)
        {
            bool exit = false;
            Unit player = new Unit()
            {
                Hp = 10,
                Str = 1,
                Vit = 1,
                Dex = 1,
                Agi = 1
            };

            AddSkills(player);
            while (exit == false)
            {
               Skill(player);
               player.DisplayStats();
               Console.WriteLine("Press 'E'|| 'e' to exit");
               char answer = Console.ReadKey().KeyChar;
                if(answer == 'E' || answer == 'e')
                {
                    exit = true;
                }
            }
           
        }

        static void AddSkills(Unit unit)
        {
            Heal heal = new Heal()
            {
                Name = "Heal"
            };
            Might might = new Might()
            {
                Name = "Might"
            };
            IronSkin ironSkin = new IronSkin()
            {
                Name = "Iron Skin"
            };
            Concentration concentration = new Concentration()
            {
                Name = "Concentration"
            };
            Haste haste = new Haste()
            {
                Name = "Haste"
            };
            for (int x = 0; x < 5; x++)
            {
                unit.skillList.Add(heal);
                unit.skillList.Add(might);
                unit.skillList.Add(ironSkin);
                unit.skillList.Add(concentration);
                unit.skillList.Add(haste);
            }
        }
        static void Skill(Unit unit)
        {
            int value = rand.Next(0, 5);
            unit.CastSkill(value);
        }
    }
}
