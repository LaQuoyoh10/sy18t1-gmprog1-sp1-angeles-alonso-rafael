﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Heal : Skills
    {
        public override void UseSkill(Unit unit)
        {
            Console.WriteLine("You used " + Name);
            unit.Hp += 10;
        }
    }
}
