﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Skills
    {
        public string Name { get; set; }

        public virtual void UseSkill(Unit unit)
        {
            Console.WriteLine("You used " + Name);
        }

    }
}
