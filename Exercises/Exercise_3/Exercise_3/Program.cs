﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Exercise_3
{
    class Program
    {
       
        static void Main(string[] args)
        {
           
            Exer4();
        }

        static void Exer1()
        {
            Console.Write("Input number: ");
            int number = Int32.Parse(Console.ReadLine());
            int factorial = 1;

            if (number == 0)
            {
                factorial = 1;
            }
            Factorial(ref number, ref factorial);

            Console.WriteLine("The Factorial of " + number + " is " + factorial);
            Console.Read();
        }

        static int Factorial(ref int value, ref int factorial)
        {
           
            for (int x = 1; x < value+1; x++)
            {
                factorial *= x;
            }
        

            return factorial;
        }


        static void Exer2()
        {
            string[] items = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
            Console.Write("Input item that you like to check: ");
            string name = Console.ReadLine();
            int count = 0;
            findItem(ref name, ref count, ref items);
            Console.WriteLine("You have " + count + " " + name + "(s)");
            Console.Read();

        }

        static void findItem(ref string name, ref int count,ref string[] array)
        {
            for (int x = 0; x < 8; x++)
            {
                if (array[x] == name)
                {
                    count++;
                }
            }
        }

        static void Exer3()
        {
            Random rand = new Random();
            int[] numbers = new int[5];
            int number = 5;
            for (int x = 0; x < 5; x++)
            {
                numbers[x] = randomizeNumbers(rand);
            }

            printArray(ref numbers, ref number);
           
            Console.Read();
        }

        static void Exer4()
        {
            Random rand = new Random();
            int number = randomizeNumbers(rand);
            int[] numbers = new int[number];

            for (int x = 0; x < number; x++)
            {
                numbers[x] = randomizeNumbers(rand);
            }

            printArray(ref numbers,ref number);

            Console.Read();
        }



        static int randomizeNumbers(Random random)
        {
            
            int value;
      
            value = random.Next(1,100);
            return value;
        }
        static void printArray(ref int[] array,ref int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                Console.Write(array[i] + ",");

            }
        }

    }
}
